package keybindings

import (
	"fmt"
	"log"
	"strings"
	"unicode"
	"unicode/utf8"

	"gioui.org/io/key"
	"gitlab.com/microo8/photon/internal/states"
)

type Callback func() error

type State struct {
	Key       rune
	Modifiers key.Modifiers
}

func (s State) String() string {
	return s.Modifiers.String() + string(s.Key)
}

type States []State

func (ss States) String() string {
	var idents string
	for _, s := range ss {
		idents += s.String()
	}
	return idents
}

func newState(e key.Event) State {
	var mod key.Modifiers
	switch {
	case e.Modifiers.Contain(key.ModCtrl):
		mod = key.ModCtrl
	case e.Modifiers.Contain(key.ModCommand):
		mod = key.ModCommand
	case e.Modifiers.Contain(key.ModShift):
		mod = key.ModShift
	case e.Modifiers.Contain(key.ModAlt):
		mod = key.ModAlt
	case e.Modifiers.Contain(key.ModSuper):
		mod = key.ModSuper
	}
	r, _ := utf8.DecodeRuneInString(e.Name)
	r = unicode.ToLower(r)
	return State{Key: r, Modifiers: mod}
}

func parseState(s string) (string, State, error) {
	ns, mod := modPrefix(s)
	if len(ns) == 0 {
		return "", State{}, fmt.Errorf("not valid keybinding string: %s", s)
	}
	r, size := utf8.DecodeRuneInString(ns)
	r = unicode.ToLower(r)
	return ns[size:], State{Key: r, Modifiers: mod}, nil
}

func modPrefix(s string) (string, key.Modifiers) {
	switch {
	case strings.HasPrefix(s, "<ctrl>"):
		return s[6:], key.ModCtrl
	case strings.HasPrefix(s, "<command>"):
		return s[9:], key.ModCommand
	case strings.HasPrefix(s, "<shift>"):
		return s[7:], key.ModShift
	case strings.HasPrefix(s, "<alt>"):
		return s[5:], key.ModAlt
	case strings.HasPrefix(s, "<super>"):
		return s[7:], key.ModSuper
	default:
		return s, 0
	}
}

func parseStates(s string) (States, error) {
	var ss States
	for {
		if len(s) == 0 {
			break
		}
		var err error
		var state State
		s, state, err = parseState(s)
		if err != nil {
			return nil, err
		}
		ss = append(ss, state)
	}
	return ss, nil
}

type Registry struct {
	currentLayout CurrentLayout
	reg           map[states.StateEnum]map[string]Callback
	currentState  States
	repeat        int
}

//Function type that returns the layout that is currently used
type CurrentLayout func() states.StateEnum

func New(cl CurrentLayout) *Registry {
	return &Registry{
		reg:           make(map[states.StateEnum]map[string]Callback),
		currentLayout: cl,
	}
}

func (kbr *Registry) Add(layout states.StateEnum, keyString string, callback Callback) {
	if _, ok := kbr.reg[layout]; !ok {
		kbr.reg[layout] = make(map[string]Callback)
	}
	ss, err := parseStates(keyString)
	if err != nil {
		log.Printf("ERROR: parsing keybinding string (%s): %s", keyString, err)
		return
	}
	kbr.reg[layout][ss.String()] = callback
}

func (kbr *Registry) Run(e key.Event) {
	if e.State != key.Release {
		return
	}
	cl := kbr.currentLayout()
	reg, ok := kbr.reg[cl]
	if !ok {
		return
	}
	s := newState(e)
	if s.Modifiers == 0 && unicode.IsDigit(s.Key) && len(kbr.currentState) == 0 {
		kbr.repeat = kbr.repeat*10 + (int(s.Key) - '0')
	}
	kbr.currentState = append(kbr.currentState, s)
	ident := kbr.currentState.String()
	var hasPrefix bool
	var callback Callback
	for k, c := range reg {
		if !strings.HasPrefix(k, ident) {
			continue
		}
		hasPrefix = true
		if len(ident) == len(k) {
			callback = c
			break
		}
	}
	if !hasPrefix {
		kbr.currentState = nil
		return
	}
	if callback == nil {
		return
	}
	kbr.currentState = nil
	if kbr.repeat == 0 {
		if err := callback(); err != nil {
			log.Println("ERROR:", err)
		}
		return
	}
	for i := 0; i < kbr.repeat; i++ {
		if err := callback(); err != nil {
			log.Println("ERROR:", err)
			break
		}
	}
	kbr.repeat = 0
}
