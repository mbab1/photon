package main

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	lua "github.com/yuin/gopher-lua"
	"gitlab.com/microo8/photon/internal/events"
	"gitlab.com/microo8/photon/internal/inputs"
	"gitlab.com/microo8/photon/internal/keybindings"
	"gitlab.com/microo8/photon/internal/media"
	"gitlab.com/microo8/photon/internal/states"
)

var luaState *lua.LState

func LoadPlugins() error {
	plugins, err := findPlugins()
	if err != nil {
		return fmt.Errorf("finding plugins: %w", err)
	}
	if len(plugins) == 0 {
		return nil
	}
	initLuaState()
	for _, pluginPath := range plugins {
		if err := luaState.DoFile(pluginPath); err != nil {
			return fmt.Errorf("loading plugin: %s\n%s", pluginPath, err)
		}
	}
	return nil
}

func findPlugins() ([]string, error) {
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	pluginsDir := filepath.Join(usr.HomeDir, ".config", "photon", "plugins")
	if _, err := os.Stat(pluginsDir); os.IsNotExist(err) {
		return nil, nil
	}
	des, err := os.ReadDir(pluginsDir)
	if err != nil {
		return nil, err
	}
	var plugins []string
	for _, de := range des {
		if de.IsDir() || !strings.HasSuffix(de.Name(), ".lua") {
			continue
		}
		plugins = append(plugins, filepath.Join(pluginsDir, de.Name()))
	}
	return plugins, nil
}

func initLuaState() {
	luaState = lua.NewState()
	media.Loader(luaState)
	cardsLoader(luaState)
	luaState.PreloadModule("photon", photonLoader)
	luaState.PreloadModule("photon.events", events.Loader)
	luaState.PreloadModule("photon.feedInputs", inputs.Loader(feedInputs))
	luaState.PreloadModule("photon.keybindings", keybindings.Loader(keyBindings))
}

func photonLoader(L *lua.LState) int {
	var exports = map[string]lua.LGFunction{
		"state": photonState,
	}
	mod := L.SetFuncs(L.NewTable(), exports)
	registerTypeSelectedCard(L)
	L.SetField(mod, "Normal", lua.LNumber(states.Normal))
	L.SetField(mod, "Article", lua.LNumber(states.Article))
	L.SetField(mod, "Search", lua.LNumber(states.Search))
	L.SetField(mod, "cards", newCards(&cards, L))
	L.SetField(mod, "visibleCards", newCards(&visibleCards, L))
	L.SetField(mod, "selectedCard", newSelectedCard(L))
	L.Push(mod)

	return 1
}

func photonState(L *lua.LState) int {
	L.Push(lua.LNumber(State()))
	return 1
}

const luaSelectedCardTypeName = "photon.selectedCardType"

func registerTypeSelectedCard(L *lua.LState) {
	var selectedCardMethods = map[string]lua.LGFunction{
		"moveRight": func(L *lua.LState) int { selectedCard.MoveRight(); return 0 },
		"moveLeft":  func(L *lua.LState) int { selectedCard.MoveLeft(); return 0 },
		"moveUp":    func(L *lua.LState) int { selectedCard.MoveUp(); return 0 },
		"moveDown":  func(L *lua.LState) int { selectedCard.MoveDown(); return 0 },
		"posX": func(L *lua.LState) int {
			if L.GetTop() == 2 {
				selectedCard.Pos.X = L.CheckInt(2)
				return 0
			}
			L.Push(lua.LNumber(selectedCard.Pos.X))
			return 1
		},
		"posY": func(L *lua.LState) int {
			if L.GetTop() == 2 {
				selectedCard.Pos.Y = L.CheckInt(2)
				return 0
			}
			L.Push(lua.LNumber(selectedCard.Pos.X))
			return 1
		},
		"card": func(L *lua.LState) int {
			L.Push(newCard(selectedCard.Card, L))
			return 1
		},
	}
	newClass := L.SetFuncs(L.NewTable(), selectedCardMethods)
	mt := L.NewTypeMetatable(luaSelectedCardTypeName)
	L.SetField(mt, "__index", newClass)
}

func newSelectedCard(L *lua.LState) *lua.LUserData {
	ud := L.NewUserData()
	ud.Value = selectedCard
	L.SetMetatable(ud, L.GetTypeMetatable(luaSelectedCardTypeName))
	return ud
}
