package main

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
	"unicode"

	"gioui.org/app"
	"gioui.org/font/gofont"
	"gioui.org/io/pointer"
	"gioui.org/layout"
	"gioui.org/op/paint"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"github.com/cixtor/readability"
	"github.com/pkg/browser"
	"gitlab.com/microo8/richtext"
	"golang.org/x/exp/shiny/materialdesign/icons"
	"golang.org/x/net/html"
)

var (
	contentLink    *widget.Icon
	fontCollection = gofont.Collection()
	shaper         = text.NewCache(fontCollection)
)

func init() {
	var err error
	contentLink, err = widget.NewIcon(icons.ContentLink)
	if err != nil {
		log.Panic(err)
	}
}

type Article struct {
	*readability.Article
	Link           string
	RichContent    richtext.TextObjects
	MaximizedImage bool
	linkClickable  widget.Clickable
	list           *layout.List
	topImage       image.Image
}

func NewArticle(link string, client *http.Client) (*readability.Article, error) {
	req, err := http.NewRequest("GET", link, nil)
	if err != nil {
		return nil, err
	}
	uri, err := url.Parse(link)
	if err != nil {
		return nil, err
	}

	if client == nil {
		client = &http.Client{
			Timeout: time.Second * 10,
		}
		req.Header.Set("cookie", "__cfduid=d73722d8bb11742b3676371f1c97f19d11517447820")
		req.Header.Set("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36")
	}
	req.Header.Set("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
	req.Header.Set("accept-language", "en-US,en;q=0.9")
	req.Header.Set("host", uri.Host)
	req.Header.Set("authority", uri.Host)
	req.Header.Set("pragma", "no-cache")
	req.Header.Set("connection", "keep-alive")
	req.Header.Set("cache-control", "no-cache")
	req.Header.Set("upgrade-insecure-requests", "1")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if mime := resp.Header.Get("Content-Type"); !isValidContentType(mime) {
		return nil, fmt.Errorf("invalid content-type `%s`", mime)
	}

	a, err := readability.New().Parse(resp.Body, link)
	if err != nil {
		return nil, err
	}
	return &a, nil
}

func ParseArticleContent(article *readability.Article) (richtext.TextObjects, error) {
	var buf bytes.Buffer
	node, err := html.Parse(strings.NewReader(article.Content))
	if err != nil {
		return nil, err
	}
	html.Render(&buf, node)
	log.Printf("%#v", buf.String())
	return parseArticleContent(node)
}

func parseArticleContent(node *html.Node) (richtext.TextObjects, error) {
	var rt richtext.TextObjects
	for node := node.FirstChild; node != nil; node = node.NextSibling {
		switch node.Data {
		case "html", "body", "header", "form":
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			rt = append(rt, subrt...)
		case "p", "section":
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			rt = append(rt, subrt...)
			rt = append(rt, &richtext.TextObject{
				Font:    fontCollection[0].Font,
				Size:    unit.Dp(18),
				Content: "\n\n",
			})
		case "span":
			color := theme.Fg
			size := unit.Dp(18)
			for _, attr := range node.Attr {
				if attr.Key == "itemprop" && attr.Val == "description" {
					color.R -= 0x11
					color.G -= 0x11
					color.B -= 0x11
					size = unit.Dp(21)
				}
			}
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			for _, to := range subrt {
				to.Color = color
				to.Size = size
			}
			rt = append(rt, subrt...)
		case "a":
			font := fontCollection[0].Font
			font.Weight = text.Bold
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			var href string
			for _, attr := range node.Attr {
				if attr.Key == "href" {
					href = attr.Val
				}
			}
			for _, to := range subrt {
				to.Font = font
				to.Color = theme.ContrastFg
				to.Clickable = true
				to.SetMetadata("href", href)
			}
			rt = append(rt, subrt...)
		case "i", "em", "blockquote":
			font := fontCollection[0].Font
			font.Style = text.Italic
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			for _, to := range subrt {
				to.Font = font
			}
			rt = append(rt, subrt...)
		case "strong", "b":
			font := fontCollection[0].Font
			font.Weight = text.Bold
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			for _, to := range subrt {
				to.Font = font
			}
			rt = append(rt, subrt...)
		case "small":
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			for _, to := range subrt {
				to.Size = unit.Dp(14)
			}
			rt = append(rt, subrt...)
		case "h1", "h2", "h3":
			font := fontCollection[0].Font
			font.Weight = text.Bold
			size := float32(28)
			switch node.Data {
			case "h2":
				size = 26
			case "h3":
				size = 24
			}
			subrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			for _, to := range subrt {
				to.Font = font
				to.Size = unit.Dp(size)
			}
			rt = append(rt, subrt...)
			rt = append(rt, &richtext.TextObject{
				Font:    fontCollection[0].Font,
				Size:    unit.Dp(18),
				Content: "\n\n",
			})
		case "div":
			divrt, err := parseArticleContent(node)
			if err != nil {
				return nil, fmt.Errorf("parsing node <%s>: %w", node.Data, err)
			}
			rt = append(rt, divrt...)
		case "svg", "img", "meta", "head", "hr":
		default:
			if node != nil && node.Type == html.TextNode {
				rt = append(rt, &richtext.TextObject{
					Font:    fontCollection[0].Font,
					Color:   theme.Fg,
					Size:    unit.Dp(18),
					Content: strings.TrimSpace(node.Data),
				})
				continue
			}
			log.Println("<"+node.Data+">", node.FirstChild)
		}
	}
	//add spaces between TextObjects
	if len(rt) > 1 {
		prev := rt[len(rt)-2]
		contentRunes := []rune(prev.Content)
		if len(contentRunes) > 0 && !unicode.IsSpace(contentRunes[len(contentRunes)-1]) {
			prev.Content += " "
		}
	}
	return rt, nil
}

func isValidContentType(mime string) bool {
	switch mime {
	case "text/html":
		return true
	case "application/rss+xml":
		return true
	case "text/html;charset=utf-8":
		return true
	case "text/html;charset=UTF-8":
		return true
	case "text/html; charset=utf-8":
		return true
	case "text/html; charset=UTF-8":
		return true
	case "text/html; charset=iso-8859-1":
		return true
	case "text/html; charset=ISO-8859-1":
		return true
	case "text/html; charset=\"utf-8\"":
		return true
	case "text/html; charset=\"UTF-8\"":
		return true
	case "text/html;;charset=UTF-8":
		return true
	case "text/plain; charset=utf-8":
		return true
	case "text/plain; charset=UTF-8":
		return true
	case "application/rss+xml; charset=utf-8":
		return true
	default:
		return false
	}
}

func (a *Article) Layout(gtx layout.Context, th *material.Theme, w *app.Window) layout.Dimensions {
	if a.list == nil {
		a.list = &layout.List{Axis: layout.Vertical}
	}
	if a.linkClickable.Clicked() {
		go browser.OpenURL(a.Link)
	}

	var flexChilds []layout.FlexChild

	flexChilds = append(
		flexChilds,
		//header
		layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				return layout.Flex{}.Layout(
					gtx,
					layout.Flexed(1,
						material.IconButton(th, &a.linkClickable, contentLink).Layout,
					),
				)
			},
		),
		//title
		layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				return layout.Inset{
					Top:    unit.Dp(20),
					Bottom: unit.Dp(20),
				}.Layout(gtx, material.H5(th, openedArticle.Title).Layout)
			},
		),
	)
	if a.topImage != nil {
		flexChilds = append(
			flexChilds,
			layout.Rigid(a.topImageLayout),
		)
	}
	flexChilds = append(
		flexChilds,
		layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				return layout.Inset{
					Top: unit.Dp(20),
				}.Layout(gtx,
					func(gtx layout.Context) layout.Dimensions {
						return a.list.Layout(gtx, 1,
							func(gtx layout.Context, rowIndex int) layout.Dimensions {
								return a.RichContent.Layout(gtx, shaper)
							},
						)
					},
				)
			},
		),
	)
	paint.Fill(gtx.Ops, color.NRGBA{
		A: 0xea,
		R: 0x12,
		G: 0x12,
		B: 0x12,
	})

	return layout.Inset{
		Top:    unit.Dp(50),
		Bottom: unit.Dp(50),
		Left:   unit.Px((float32(gtx.Constraints.Max.X) - unit.Dp(800).V) / 2),
		Right:  unit.Px((float32(gtx.Constraints.Max.X) - unit.Dp(800).V) / 2),
	}.Layout(
		gtx,
		func(gtx layout.Context) layout.Dimensions {
			return Surface{Elevation: unit.Dp(2)}.Layout(
				gtx,
				th,
				func(gtx layout.Context) layout.Dimensions {
					return layout.UniformInset(unit.Dp(30)).Layout(gtx,
						func(gtx layout.Context) layout.Dimensions {
							return layout.Flex{Axis: layout.Vertical}.Layout(gtx, flexChilds...)
						},
					)
				},
			)
		},
	)
}

func (a *Article) topImageLayout(gtx layout.Context) layout.Dimensions {
	// Process events that arrived between the last frame and this one.
	for _, ev := range gtx.Queue.Events(openedArticle) {
		if x, ok := ev.(pointer.Event); ok {
			switch x.Type {
			case pointer.Press:
				a.MaximizedImage = !a.MaximizedImage
			}
		}
	}
	scaleImage := 0.4
	if a.MaximizedImage {
		scaleImage = 1
	}
	gtx.Constraints.Max.Y = int(float64(gtx.Constraints.Max.Y) * scaleImage)
	return layout.Flex{Spacing: layout.SpaceAround}.Layout(
		gtx,
		layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				dims := widget.Image{
					Src: paint.NewImageOp(a.topImage),
					Fit: widget.Contain,
				}.Layout(gtx)
				pointer.Rect(
					image.Rect(0, 0, dims.Size.X, dims.Size.Y),
				).Add(gtx.Ops)
				pointer.InputOp{
					Tag:   openedArticle,
					Types: pointer.Press,
				}.Add(gtx.Ops)
				return dims
			},
		),
	)
}
