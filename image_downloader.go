package main

import (
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"net/http"
	"runtime"
	"sync"

	_ "golang.org/x/image/webp"
)

type ImgDownloader struct {
	client    *http.Client
	receiveCh chan imgDownloadReq
	imgCache  sync.Map
}

type imgDownloadReq struct {
	URL      string
	Callback func(image.Image)
}

type ImgDownloaderOption func(d *ImgDownloader)

func NewImgDownloader(options ...ImgDownloaderOption) *ImgDownloader {
	d := &ImgDownloader{receiveCh: make(chan imgDownloadReq, 1024)}
	for _, o := range options {
		o(d)
	}
	reqCh := make(chan imgDownloadReq, 1024)
	//receiver
	go func() {
		for req := range d.receiveCh {
			if i, ok := d.imgCache.Load(req.URL); ok {
				if i != nil {
					if req.Callback != nil {
						req.Callback(i.(image.Image))
					}
				}
				continue
			}
			d.imgCache.Store(req.URL, nil)
			reqCh <- req
		}
	}()
	//download workers
	for i := 0; i < runtime.NumCPU(); i++ {
		go func() {
			client := d.client
			if client == nil {
				client = http.DefaultClient
			}
			for req := range reqCh {
				r, err := http.NewRequest("GET", req.URL, nil)
				if err != nil {
					log.Println("ERROR: creating request for image:", err)
					continue
				}
				resp, err := client.Do(r)
				if err != nil {
					log.Println("ERROR: downloading image:", err)
					continue
				}
				defer resp.Body.Close()
				i, _, err := image.Decode(resp.Body)
				if err != nil {
					log.Println("ERROR: decoding image:", err, req.URL)
					continue
				}
				d.imgCache.Store(req.URL, i)
				if req.Callback != nil {
					req.Callback(i)
				}
			}
		}()
	}
	return d
}

func WithClient(client *http.Client) ImgDownloaderOption {
	return func(d *ImgDownloader) {
		d.client = client
	}
}

func (d *ImgDownloader) Download(url string, callback func(image.Image)) {
	img, ok := d.imgCache.Load(url)
	if !ok || img == nil {
		d.receiveCh <- imgDownloadReq{
			URL:      url,
			Callback: callback,
		}
	} else {
		if callback != nil {
			callback(img.(image.Image))
		}
	}
}

func (d *ImgDownloader) GetImg(url string) (image.Image, bool) {
	i, ok := d.imgCache.Load(url)
	if !ok || i == nil {
		return nil, false
	}
	return i.(image.Image), true
}
