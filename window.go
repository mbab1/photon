package main

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math"
	"strings"

	"gioui.org/app"
	"gioui.org/f32"
	"gioui.org/font/gofont"
	"gioui.org/io/key"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/clip"
	"gioui.org/op/paint"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"github.com/pkg/browser"
	"gitlab.com/microo8/photon/internal/keybindings"
	"gitlab.com/microo8/photon/internal/states"
	"golang.design/x/clipboard"
)

var (
	cards         Cards   //All the Cards
	visibleCards  Cards   //Cards that are filtered by the search query
	onScreenCards []*Card //Cards that are now visible on the screen list

	keyBindings = keybindings.New(State)

	theme             = material.NewTheme(gofont.Collection())
	list              = &layout.List{Axis: layout.Vertical}
	openedArticle     = &Article{}
	ncols             = 5
	searchEditor      *widget.Editor
	searchEditorFocus bool
)

func init() {
	theme.Palette = material.Palette{
		Fg:         rgb(0xffffff),
		Bg:         rgb(0x121212),
		ContrastBg: rgb(0x343434),
		ContrastFg: rgb(0xffed4c),
	}
}

func State() states.StateEnum {
	switch {
	case openedArticle.Article != nil:
		return states.Article
	case searchEditor != nil && searchEditorFocus:
		return states.Search
	default:
		return states.Normal
	}
}

func guiLoop(w *app.Window) error {
	var ops op.Ops
	for {
		select {
		case e := <-w.Events():
			switch e := e.(type) {
			case system.DestroyEvent:
				return e.Err
			case key.Event:
				keyBindings.Run(e)
			case system.FrameEvent:
				gtx := layout.NewContext(&ops, e)
				paint.Fill(gtx.Ops, theme.Bg)
				drawWindow(gtx, theme, w)
				e.Frame(gtx.Ops)
			}
		}
	}
}

func drawWindow(gtx layout.Context, th *material.Theme, w *app.Window) layout.Dimensions {
	s := State()
	switch {
	case s == states.Normal && searchEditor == nil:
		return cardList(gtx, th, w)
	case s == states.Article:
		if to := openedArticle.RichContent.Clicked(); to != nil {
			href := to.GetMetadata("href")
			go browser.OpenURL(href)
		}
		return layout.Stack{}.Layout(gtx,
			layout.Expanded(
				func(gtx layout.Context) layout.Dimensions {
					return cardList(gtx, th, w)
				},
			),
			layout.Expanded(
				func(gtx layout.Context) layout.Dimensions {
					return openedArticle.Layout(gtx, th, w)
				},
			),
		)
	case s == states.Search || (s == states.Normal && searchEditor != nil):
		query := searchEditor.Text()
		if query == "" {
			searchEditor = nil
			return cardList(gtx, th, w)
		}
		if len(searchEditor.Events()) > 0 {
			filterCards(query)
		}
		return layout.Stack{}.Layout(gtx,
			layout.Expanded(
				func(gtx layout.Context) layout.Dimensions {
					return cardList(gtx, th, w)
				},
			),
			layout.Stacked(
				func(gtx layout.Context) layout.Dimensions {
					cgtx := gtx
					macro := op.Record(gtx.Ops)
					dims := material.Editor(th, searchEditor, "").Layout(cgtx)
					call := macro.Stop()
					sz := dims.Size
					op.Offset(
						f32.Point{X: 0, Y: float32(gtx.Constraints.Max.Y - sz.Y)},
					).Add(gtx.Ops)
					r := f32.Rect(0, 0, float32(gtx.Constraints.Max.X), float32(sz.Y+4))
					clip.UniformRRect(r, 0).Add(gtx.Ops)
					paint.Fill(gtx.Ops, color.NRGBA{A: 0xff, R: 0x23, G: 0x23, B: 0x23})
					call.Add(gtx.Ops)
					return dims
				},
			),
		)
	default:
		panic("not known state")
	}
}

func cardList(gtx layout.Context, th *material.Theme, w *app.Window) layout.Dimensions {
	onScreenCards = nil
	maxVisibleRow := list.Position.First + list.Position.Count
	//download images for items in the next rows in List
	defer func() {
		for rowIndex := maxVisibleRow + 1; rowIndex < maxVisibleRow+1+list.Position.Count; rowIndex++ {
			for i := 0; i < ncols && (rowIndex*ncols+i) < len(visibleCards); i++ {
				card := visibleCards[rowIndex*ncols+i]
				if card.Item.Image != nil {
					imgDownloader.Download(card.Item.Image.URL, card.saveImage())
				}
			}
		}
	}()
	defer func() {
		if selectedCard.Pos.Y > list.Position.First && selectedCard.Pos.Y < maxVisibleRow {
			return
		}
		if list.Position.Count == 0 {
			return
		}
		if selectedCard.Pos.Y < list.Position.First {
			selectedCard.Pos.Y = list.Position.First
		} else if selectedCard.Pos.Y > maxVisibleRow {
			selectedCard.Pos.Y = maxVisibleRow
		}
		selectedCard.Refresh()
		w.Invalidate()
	}()
	cardSize := image.Point{
		X: gtx.Constraints.Max.X / ncols,
		Y: gtx.Constraints.Max.X / ncols,
	}
	if searchEditorFocus && searchEditor != nil && !searchEditor.Focused() {
		searchEditor.Focus()
	}
	if !searchEditorFocus && searchEditor != nil && searchEditor.Focused() {
		key.FocusOp{}.Add(gtx.Ops)
	}
	return list.Layout(
		gtx,
		int(math.Ceil(float64(len(visibleCards))/float64(ncols))),
		func(gtx layout.Context, rowIndex int) layout.Dimensions {
			var flexChilds []layout.FlexChild
			for i := 0; i < ncols && (rowIndex*ncols+i) < len(visibleCards); i++ {
				card := visibleCards[rowIndex*ncols+i]
				if card.Item.Image != nil {
					imgDownloader.Download(card.Item.Image.URL, card.saveImage())
				}
				onScreenCards = append(onScreenCards, card)
				flexChilds = append(flexChilds, layout.Rigid(
					func(gtx layout.Context) layout.Dimensions {
						gtx.Constraints.Min = cardSize
						gtx.Constraints.Max = cardSize
						return card.Layout(gtx, th)
					},
				))
			}
			return layout.Flex{}.Layout(gtx, flexChilds...)
		},
	)
}

func defaultKeyBindings(w *app.Window) {
	//NormalState
	keyBindings.Add(states.Normal, key.NameEnter, func() error {
		selectedCard.OpenArticle()
		return nil
	})
	keyBindings.Add(states.Normal, "⏎", func() error {
		selectedCard.OpenArticle()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>"+key.NameEnter, func() error {
		selectedCard.RunMedia()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>⏎", func() error {
		selectedCard.RunMedia()
		return nil
	})
	keyBindings.Add(states.Normal, "<alt>"+key.NameEnter, func() error {
		selectedCard.OpenBrowser()
		return nil
	})
	keyBindings.Add(states.Normal, "<alt>⏎", func() error {
		selectedCard.OpenBrowser()
		return nil
	})
	keyBindings.Add(states.Normal, key.NameEscape, func() error {
		if searchEditor == nil {
			return nil
		}
		searchEditor = nil
		visibleCards = cards
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>=", func() error {
		ncols++
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>-", func() error {
		if ncols > 1 {
			ncols--
		}
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "/", func() error {
		if searchEditor != nil && !searchEditor.Focused() {
			searchEditorFocus = true
			w.Invalidate()
			return nil
		}
		searchEditor = &widget.Editor{SingleLine: true, Submit: true}
		searchEditor.SetText("/")
		searchEditor.SetCaret(1, 1)
		searchEditorFocus = true
		w.Invalidate()
		return nil
	})
	//copy item link
	keyBindings.Add(states.Normal, "yy", func() error {
		if selectedCard == nil {
			return nil
		}
		clipboard.Write(clipboard.FmtText, []byte(selectedCard.Item.Link))
		return nil
	})
	//copy item image
	keyBindings.Add(states.Normal, "yi", func() error {
		if selectedCard == nil {
			return nil
		}
		if selectedCard.ItemImage == nil {
			return nil
		}
		var buf bytes.Buffer
		if err := png.Encode(&buf, selectedCard.ItemImage); err != nil {
			return fmt.Errorf("encoding image: %w", err)
		}
		clipboard.Write(clipboard.FmtImage, buf.Bytes())
		return nil
	})
	//move selectedCard
	keyBindings.Add(states.Normal, "h", func() error {
		selectedCard.MoveLeft()
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "l", func() error {
		selectedCard.MoveRight()
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "j", func() error {
		selectedCard.MoveDown()
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "k", func() error {
		selectedCard.MoveUp()
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>d", func() error {
		list.Position.First += int(float32(list.Position.Count) / 2)
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>u", func() error {
		list.Position.First -= int(float32(list.Position.Count) / 2)
		if list.Position.First < 0 {
			list.Position.First = 0
		}
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>f", func() error {
		list.Position.First += list.Position.Count
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<ctrl>b", func() error {
		list.Position.First -= list.Position.Count
		if list.Position.First < 0 {
			list.Position.First = 0
		}
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "gg", func() error {
		list.Position.First = 0
		list.Position.Offset = 0
		selectedCard.Pos.Y = 0
		selectedCard.Refresh()
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Normal, "<shift>g", func() error {
		list.Position.First = len(visibleCards) - list.Position.Count
		selectedCard.Pos.Y = len(visibleCards)/ncols - 1
		selectedCard.Refresh()
		w.Invalidate()
		return nil
	})

	//SearchState
	keyBindings.Add(states.Search, "⏎", func() error {
		searchEditorFocus = false
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Search, key.NameEnter, func() error {
		searchEditorFocus = false
		w.Invalidate()
		return nil
	})
	keyBindings.Add(states.Search, key.NameEscape, func() error {
		searchEditor = nil
		visibleCards = cards
		w.Invalidate()
		return nil
	})

	//ArticleState
	keyBindings.Add(states.Article, key.NameEscape, func() error {
		openedArticle = &Article{}
		w.Invalidate()
		return nil
	})
}

func filterCards(query string) {
	defer func() {
		selectedCard.Pos.X, selectedCard.Pos.Y = 0, 0
		if len(visibleCards) == 0 {
			selectedCard.Card = nil
		}
	}()
	query = strings.ToLower(strings.TrimPrefix(query, "/"))
	if query == "" {
		visibleCards = cards
		return
	}
	visibleCards = nil
	for _, card := range cards {
		if strings.Contains(strings.ToLower(card.Item.Title), query) {
			visibleCards = append(visibleCards, card)
			continue
		}
		if strings.Contains(strings.ToLower(card.Item.Description), query) {
			visibleCards = append(visibleCards, card)
			continue
		}
		if strings.Contains(strings.ToLower(card.Feed.Title), query) {
			visibleCards = append(visibleCards, card)
			continue
		}
		if card.Item.Author != nil {
			if strings.Contains(strings.ToLower(card.Item.Author.Name), query) {
				visibleCards = append(visibleCards, card)
				continue
			}
		}
	}
}
