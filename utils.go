package main

import (
	"image/color"
)

func rgb(c uint32) color.NRGBA {
	return argb(0xff000000 | c)
}

func argb(c uint32) color.NRGBA {
	return color.NRGBA{
		A: uint8(c >> 24),
		R: uint8(c >> 16),
		G: uint8(c >> 8),
		B: uint8(c),
	}
}

func shadeColor(c color.NRGBA, percent int) color.NRGBA {
	return color.NRGBA{
		A: c.A,
		R: minUint8(uint8(int(c.R)*(100+percent)/100), 255),
		G: minUint8(uint8(int(c.G)*(100+percent)/100), 255),
		B: minUint8(uint8(int(c.B)*(100+percent)/100), 255),
	}
}

func maxUint8(a, b uint8) uint8 {
	if a > b {
		return a
	}
	return b
}

func minUint8(a, b uint8) uint8 {
	if a < b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
